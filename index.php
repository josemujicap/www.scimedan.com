<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>SciMeDan // Cesartevisual</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-rubiklato.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body class=" ">
        <a id="start"></a>
        <section class="bar bar-3 bar--sm bg--secondary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                      <!--  <div class="bar__module">
                            <span class="type-fade">SciMeDan</span>
                        </div> -->
                    </div>
                    <div class="col-lg-6 text-right text-left-xs text-left-sm">
                        <div class="bar__module">
                            <ul class="menu-horizontal">

                                <li>
                                    <a href="eng.html">
                                        <img alt="Image" class="flag" src="img/flag-2.png" />
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end bar-->
        <div class="notification pos-right pos-top side-menu bg--white" data-notification-link="side-menu" data-animation="from-right">
            <div class="side-menu__module">
                <a class="btn btn--icon bg--facebook block" href="#">
                    <span class="btn__text">
                        <i class="socicon-facebook"></i>
                        Sign up with Facebook
                    </span>
                </a>
                <a class="btn btn--icon bg--dark block" href="#">
                    <span class="btn__text">
                        <i class="socicon-mail"></i>
                        Sign up with Email
                    </span>
                </a>
            </div>
            <!--end module-->
            <hr>
            <div class="side-menu__module">
                <span class="type--fine-print float-left">Already have an account?</span>
                <a class="btn type--uppercase float-right" href="#">
                    <span class="btn__text">Login</span>
                </a>
            </div>
            <!--end module-->
            <hr>
            <div class="side-menu__module">
                <ul class="list--loose list--hover">
                    <li>
                        <a href="#">
                            <span class="h5">About Stack</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="h5">Careers</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="h5">Investors</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="h5">Locations</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="h5">Contact</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!--end module-->
            <hr>
            <div class="side-menu__module">
                <ul class="social-list list-inline list--hover">
                    <li>
                        <a href="#">
                            <i class="socicon socicon-google icon icon--xs"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="socicon socicon-twitter icon icon--xs"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="socicon socicon-facebook icon icon--xs"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="socicon socicon-instagram icon icon--xs"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="socicon socicon-pinterest icon icon--xs"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="notification pos-top pos-right search-box bg--white border--bottom" data-animation="from-top" data-notification-link="search-box">
            <form>
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-8">
                        <input type="search" name="query" placeholder="Type search query and hit enter" />
                    </div>
                </div>
                <!--end of row-->
            </form>
        </div>
        <!--end of notification-->
        <div class="nav-container ">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <a href="index.html">
                                <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                <img class="logo logo-light" alt="logo" src="img/logo-light.png" />
                            </a>
                        </div>
                        <div class="col-9 col-md-10 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </div>
            <!--end bar-->
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs bar--absolute">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-1 col-md-2 hidden-xs">
                            <div class="bar__module">
                                <a href="index.html">
                                    <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                    <img class="logo logo-light" alt="logo" src="img/logo-light.png" />
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                        <div class="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
                            <div class="bar__module">







                            </div>
                            <!--end module-->
                            <div class="bar__module">
                                <a class="btn btn--sm type--uppercase" href="http://blogg.scimedan.com/" target="_blank">
                                    <span class="btn__text">
                                        Blog
                                    </span>
                                </a>
                                <a class="btn btn--sm btn--primary type--uppercase" href="#contactanos">
                                    <span class="btn__text">
                                        Contáctenos
                                    </span>
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </nav>
            <!--end bar-->
        </div>
        <div class="main-container">
            <section class="cover unpad--bottom switchable switchable--switch bg--secondary text-center-xs">
                <div class="container">
                    <div class="row align-items-center justify-content-around">
                        <div class="col-md-6 col-lg-5 mt-0">
                            <h2 style="color:#62378B">
                                Bienvenidos a SciMeDan
                                <h4> Farmacocinética y estadística  </h4>
                            </h2>
                            <p class="lead">
                                Nuestra misión es ayudarle a encontrar información en sus datos.

                              <strong>  Planificación, seguimiento y análisis. </strong>
                            </p>
                          <!--  <a class="btn btn--primary type--uppercase" href="mailto:info@scimedan.com">
                                <span class="btn__text">
                                  Escríbenos
                                </span>
                            </a> -->

                        </div>
                        <div class="col-md-6">
                            <img alt="Image" src="img/avatar-large-3.png" />
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>


            <!-- <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="feature feature-5 boxed boxed--lg boxed--border">
                                <i class="icon icon-Pantone icon--lg"></i>
                                <div class="feature__body">
                                    <h5>Highly Customizable</h5>
                                    <p>
                                        Stack's visual style is simple yet distinct. Perfect for your next project whether it be a basic marketing site, or multi-page company presence.
                                    </p>
                                    <a href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="feature feature-5 boxed boxed--lg boxed--border">
                                <i class="icon icon-Fingerprint icon--lg"></i>
                                <div class="feature__body">
                                    <h5>Built For Startups</h5>
                                    <p>
                                        Launching an attractive website quickly and affordably is important for modern startups &mdash; Stack offers massive value with high-end styling.
                                    </p>
                                    <a href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="feature feature--featured feature-5 boxed boxed--lg boxed--border">
                                <span class="label">Free</span>
                                <i class="icon icon-Duplicate-Window icon--lg"></i>
                                <div class="feature__body">
                                    <h5>Page Builder Included</h5>
                                    <p>
                                        Construct mockups or production-ready pages in-browser with Variant Page Builder &mdash; Included FREE with every license purchase.
                                    </p>
                                    <a href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="feature feature-5 boxed boxed--lg boxed--border">
                                <i class="icon icon-Life-Safer icon--lg"></i>
                                <div class="feature__body">
                                    <h5>Friendly Support</h5>
                                    <p>
                                        You'll love the comfort that comes with six-months free support. Our dedicated support forum makes support hassle-free and efficient.
                                    </p>
                                    <a href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </section>


            <section class="feature-large feature-large-2 bg--secondary">
                <div class="container">
                    <div class="row justify-content-around">
                        <div class="col-md-4 col-lg-3">
                            <h3>Experience quality</h3>
                            <p class="lead">
                                Stack is built with customization and ease-of-use at its core &mdash; whether you're a seasoned developer or just starting out, you'll be making attractive sites faster than any traditional HTML template.
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <img alt="Image" class="border--round box-shadow-wide" src="img/landing-7.jpg" />
                        </div>
                        <div class="col-md-4 col-lg-2">
                            <hr class="short" />
                            <p>
                                Each purchase of Stack includes six months of free support, including access to our dedicated support forum. In addition to support you'll recieve lifetime updates, including new content and bug-fixes.
                            </p>
                        </div>
                    </div>
                </div>

            </section>-->





            <section class="cover height-80 imagebg text-center" data-overlay="6">
                <div class="background-image-holder">
                    <img alt="background" src="img/education-1.jpg" />
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-md-6">
                            <span class="h1">En SciMeDan buscamos:</span>
                            <p class="lead">
                              Dar soluciones creativas a problemas estadísticos y farmacocinéticos con comprensión de la naturaleza de esos problemas.
                            </p>
                            <p> Ayudar a escoger las preguntas correctas para obtener las respuestas correctas sobre lo que se quiere estudiar, lo que se quiere analizar, cuales son los resultados significativos que espera tener y orientar a escoger el camino para llegar a ello.                            </p>


                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>



            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Nuestros servicios</h2>
                            <div class="slider" data-arrows="false" data-paging="true">
                                <ul class="slides">

                                    <li class="col-md-4 col-12">
                                        <div class="feature feature-3 boxed boxed--lg boxed--border text-center">
                                            <div>   <img alt="background" src="img/icon1.png" /> </div>
                                            <h4>Diseño estadístico de proyectos</h4>
                                            <p>
                                              Análisis de resultados preliminares para seleccionar el diseño estadístico. Selección del diseño estadístico para detectar efectos relevantes y estadísticamente significativos. Cálculo del tamaño de las muestras.
                                            </p>
                                          <!--  <a href="#">
                                                Contáctanos
                                            </a> -->
                                        </div>
                                    </li>

                                    <li class="col-md-4 col-12">
                                        <div class="feature feature-3 boxed boxed--lg boxed--border text-center">
                                            <div>   <img alt="background" src="img/icon3.png" /> </div>
                                            <h4> Análisis de datos  </h4>
                                            <p> Asesoría y Seguimiento de proyectos. Asesoría sobre análisis de datos problemáticos. Asesoría en problemas de arbitraje.
                                            </p>
                                          <!--  <a href="#">
                                                Contáctanos
                                            </a> -->
                                        </div>
                                    </li>

                                    <li class="col-md-4 col-12">
                                        <div class="feature feature-3 boxed boxed--lg boxed--border text-center">
                                          <div>   <img alt="background" src="img/icon2.png" /> </div>
                                            <h4>Análisis de datos operativos</h4>
                                            <p>
                                                Búsqueda de factores no evidentes que determinan la eficiciencia institucional. Series de tiempos. Procesos markovianos y de sistemas dinámicos sensibles a las condiciones iniciales.
                                            </p>
                                        <!--    <a href="#">
                                                Contáctanos
                                            </a> -->
                                        </div>
                                    </li>

                                    <li class="col-md-4 col-12">
                                        <div class="feature feature-3 boxed boxed--lg boxed--border text-center">
                                              <div>   <img alt="background" src="img/icon4.png" /> </div>
                                            <h4>Dirigidos al sector corporativo</h4>
                                            <p>
                                              Análisis y asesoría para el diseño de estudios farmacocinéticos y farmacodinámicos. Asesoría en el diseño de pruebas clínicas. Análisis estadístico de estudios clínicos. Asesoría para el registro de fármacos.

                                            </p>
                                        <!--    <a href="#">
                                                Contáctanos
                                            </a> -->
                                        </div>
                                    </li>

                                  <!--  <li class="col-md-4 col-12">
                                        <div class="feature feature-3 boxed boxed--lg boxed--border text-center">
                                            <i class="color--primary icon icon--lg icon-Hospital-2"></i>
                                            <h4>Hospital Cover</h4>
                                            <p>
                                                Stack comes with integration for Mail Chimp and Campaign Monitor forms - ideal for modern marketing campaigns
                                            </p>
                                            <a href="#">
                                                Learn More
                                            </a>
                                        </div>
                                    </li> -->

                                </ul>
                            </div>
                            <!--end of row-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>



            <section class="text-center space--lg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-lg-8">
                            <h1>Nuestra meta</h1>
                            <p class="lead"> Trabajar con los mejores talentos de la industria para producir resultados excelentes.  </p>
                        </div>
                    </div>
                </div>
            </section>




        <!--    <section class="imagebg parallax" data-scrim-top="9">
                <div class="background-image-holder">
                    <img alt="background" src="img/education-3.jpg" />
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="cta">
                                <h2>Nuestros clientes nos recomiendan</h2>
                                <p class="lead">
                                    Working with the industries best and brightest minds to deliver outstanding results
                                </p>



                                <a class="btn btn--primary type--uppercase" href="#">
                                    <span class="btn__text">
                                        botón verde
                                    </span>
                                </a>


                            </div>
                        </div>
                    </div>
                </div>
            </section> -->



            <section class="text-center imagebg parallax" data-overlay="5">
                <div class="background-image-holder"> <img alt="background" src="img/hero-1.jpg"> </div>
                <div class="container">

<h2>Nuestros clientes nos recomiendan</h2>

                    <div class="row">

                        <div class="col-md-6 col-lg-3">
                          <div class="text-block">
                            <img alt="Image" src="img/partner-1.png" />
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3">
                          <div class="text-block">
                            <img alt="Image" src="img/partner-5.png" />
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3">
                          <div class="text-block">
                            <img alt="Image" src="img/partner-7.png" />
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3">
                          <div class="text-block">
                            <img alt="Image" src="img/partner-4.png" />
                            </div>
                        </div>



                    </div>
                </div>
            </section>




        <!--    <section class=" ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="slider slider--inline-arrows" data-arrows="true">
                                <ul class="slides">
                                    <li>
                                        <div class="testimonial row justify-content-center">
                                            <div class="col-lg-2 col-md-4 col-6 text-center">
                                                <img class="testimonial__image" alt="Image" src="img/avatar-round-1.png" />
                                            </div>
                                            <div class="col-lg-7 col-md-8 col-12">
                                                <span class="h3">&ldquo;We’ve been using Stack to prototype designs quickly and efficiently. Needless to say we’re hugely impressed by the style and value.&rdquo;
                                                </span>
                                                <h5>Maguerite Holland</h5>
                                                <span>Interface Designer &mdash; Yoke</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="testimonial row justify-content-center">
                                            <div class="col-lg-2 col-md-4 col-6 text-center">
                                                <img class="testimonial__image" alt="Image" src="img/avatar-round-4.png" />
                                            </div>
                                            <div class="col-lg-7 col-md-8 col-12">
                                                <span class="h3">&ldquo;I've been using Medium Rare's templates for a couple of years now and Stack is without a doubt their best work yet. It's fast, performant and absolutely stunning.&rdquo;
                                                </span>
                                                <h5>Lucas Nguyen</h5>
                                                <span>Freelance Designer</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="testimonial row justify-content-center">
                                            <div class="col-lg-2 col-md-4 col-6 text-center">
                                                <img class="testimonial__image" alt="Image" src="img/avatar-round-3.png" />
                                            </div>
                                            <div class="col-lg-7 col-md-8 col-12">
                                                <span class="h3">&ldquo;Variant has been a massive plus for my workflow &mdash; I can now get live mockups out in a matter of hours, my clients really love it.&rdquo;
                                                </span>
                                                <h5>Rob Vasquez</h5>
                                                <span>Interface Designer &mdash; Yoke</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </section> -->




<!--
                        <section class="unpad">
                            <div class="row--gapless row">
                                <div class="col-md-4 col-6">
                                    <a href="#" class="block">
                                        <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                                            <div class="background-image-holder">
                                                <img alt="background" src="img/education-5.jpg" />
                                            </div>
                                            <h4 class="pos-vertical-center">Future Students</h4>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4 col-6">
                                    <a href="#" class="block">
                                        <div class="feature feature-7 boxed text-center imagebg" data-overlay="5">
                                            <div class="background-image-holder">
                                                <img alt="background" src="img/education-6.jpg" />
                                            </div>
                                            <h4 class="pos-vertical-center">Current Students</h4>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4 col-6">
                                    <a href="#" class="block">
                                        <div class="feature feature-7 boxed text-center imagebg" data-overlay="5">
                                            <div class="background-image-holder">
                                                <img alt="background" src="img/education-7.jpg" />
                                            </div>
                                            <h4 class="pos-vertical-center">Alumni</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </section> -->




          <!--  <section class="text-center">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 col-lg-6">
                            <div class="cta">
                                <h2>We're always looking for talent</h2>
                                <p class="lead">
                                    Got what it takes to work with us? Great! Send us a link to your resumé or portfolio to become part of our talent pool.
                                </p>
                                <a class="btn btn--primary type--uppercase" href="#">
                                    <span class="btn__text">
                                        See Job Openings
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->



            <section id="contactanos" class="switchable text-center space--xs">
                <div class="container">
                    <div class="row justify-content-between">

                        <div class="col-lg-5 col-md-5">
                            <div class="switchable__text">
                                <h3>Barcelona, España</h3>
                                <p class="lead"> E: <a href="mailto:info@scimedan.com">info@scimedan.com</a><br> P: +34 697 66 8402 </p>
                                <p class="lead"> Llámenos, le responderemos a todas sus consultas. </p>
                                <p class="lead"> 10:00 a 19:00 (UE)  </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>




            <footer class="text-center-xs space--xs bg--dark ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <ul class="list-inline">
                                <li>
                                    <a href="#">
                                        <span class="h6 type--uppercase">Nosotros</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="h6 type--uppercase">Empleo</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="h6 type--uppercase">contacto</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                      <!--  <div class="col-md-5 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>-->
                    </div>


                    <!--end of row-->
                    <div class="row">
                        <div class="col-md-7">
                            <span class="type--fine-print">&copy;
                                <span class="update-year"></span> SciMeDan.</span>
                            <a class="type--fine-print" href="#">Privacy Policy</a>
                            <a class="type--fine-print" href="#">Legal</a>
                        </div>
                        <div class="col-md-5 text-right text-center-xs">
                            <a class="type--fine-print" href="mailto:info@scimedan.com">info@scimedan.com</a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/flickity.min.js"></script>
        <script src="js/easypiechart.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/typed.min.js"></script>
        <script src="js/datepicker.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/ytplayer.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/granim.min.js"></script>
        <script src="js/jquery.steps.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
